package com.javibar.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javibar.model.Category;
import com.javibar.repository.CategoryRepository;

@RestController
@RequestMapping("/category")
public class CategoryController {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Category> getAllCategories(){
		return categoryRepository.findAll();
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Map<String, Object> createCategory(@Valid @RequestBody Category category){
		category.set_id(ObjectId.get());
		categoryRepository.save(category);
		
		Map<String, Object> response = new LinkedHashMap<String, Object>();
	    response.put("message", "Categoría creada con éxito");
	    response.put("Category", category);
	    response.put("code", 200);
	    return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{categoryId}")
	public Category getCategoryDetails(@PathVariable("categoryId") ObjectId categoryId) {
		return categoryRepository.findBy_id(categoryId);
	}
}
