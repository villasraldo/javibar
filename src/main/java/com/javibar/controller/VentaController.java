package com.javibar.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javibar.model.Cierres;
import com.javibar.model.Venta;
import com.javibar.repository.CategoryRepository;
import com.javibar.repository.CierresRepository;
import com.javibar.repository.VentaRepository;

@RestController
@RequestMapping("/venta")
public class VentaController {
	
	@Autowired
	VentaRepository repository;
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@Autowired
	CierresRepository cierresRepository;
	
	private static Sort ASC = new Sort(Sort.Direction.ASC, "monto");
	private static Sort DESC = new Sort(Sort.Direction.DESC, "monto");
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Map<String, Object> createVenta(@Valid @RequestBody Venta venta){
		venta.set_id(ObjectId.get());
		repository.save(venta);
		Map<String, Object> response = new LinkedHashMap<String, Object>();
	    response.put("message", "Venta creada con éxito");
	    response.put("Category", venta);
	    response.put("code", 200);
	    return response;
	}
	
	@RequestMapping(value = "/daily", method = RequestMethod.POST)
	public Map<String, Object> createDaily(@Valid @RequestBody String venta) {
		System.out.println(venta);
		List<String> ventas = Arrays.asList(venta.split("%0A"));
		List<Venta> listVentas = walkThroughDaily(ventas);
		String codemax = repository.findAllByNocierreOrderByFechaDesc(getNo_Cierre(), getDate(), DESC).get(0).getCodigo();
		String codemin = repository.findAllByNocierreOrderByFechaAsc(getNo_Cierre(), getDate(), ASC).get(0).getCodigo();
		System.out.println("CODEMAX: " + codemax);
		System.out.println("CODEMIN: " + codemin);
		Double media = getMedia(listVentas);
		Double dMedia = getMedia(repository.findByCodigoAndFechaAndNocierre("D", getDate(), getNo_Cierre()));
		System.out.println("MEDIA: " + media);
		System.out.println("DMEDIA: " + dMedia);
		String max = categoryRepository.findByCodigo(codemax).getNombre(); 
		String min = categoryRepository.findByCodigo(codemin).getNombre();
		String isMedia = media < dMedia ? "SI" : "NO";

		Cierres cierre = new Cierres(new Date(), getNo_Cierre()+1);
		cierre.set_id(ObjectId.get());
		cierresRepository.save(cierre);
		Map<String, Object> response = new LinkedHashMap<String, Object>();
		response.put("data", max + "#" + min + "#" + isMedia);
		return response;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Venta> getAllVentas(){
		return repository.findAll();
	}
	
	@RequestMapping(value = "/{fecha}")
	public List<Venta> getByDate(@PathVariable("fecha") Date fecha) {
		return repository.findByFecha(fecha);
	}
	
	private List<Venta> walkThroughDaily(List<String> ventas){
		List<Venta> listVentas = new ArrayList<Venta>();
		for(String s : ventas) {
			String[] arr = s.split("\\+");
			Venta v = new Venta(arr[0], getDate(), Double.parseDouble(arr[1].replace("=", "")), getNo_Cierre());
			if(!v.getCodigo().equals("N")) {
				v.set_id(ObjectId.get());
				repository.save(v);
			}
			listVentas.add(v);
		}
		return listVentas;
	}
	
	private Double getMedia(List<Venta> ventas) {
		Double media = 0.0;
		for(Venta v : ventas) {
			media += v.getMonto();
		}
		media /= ventas.size();
		return media;
	}
	
	private static Date getDate() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	 
	    return calendar.getTime();
	}
	
	private Integer getNo_Cierre() {
		Integer no_cierre = 1;
		List<Cierres> cierres = cierresRepository.findAllByOrderByFechaDesc(new Date(), DESC);
		if(cierres != null && cierres.size() > 0) {
			no_cierre = cierres.get(0).getNocierre();
			no_cierre = no_cierre == 0 ? 1 : no_cierre;
		} else {
			Cierres cierre = new Cierres(new Date(), no_cierre);
			cierre.set_id(ObjectId.get());
			cierresRepository.save(cierre);
		}
		return no_cierre;
	}
}
