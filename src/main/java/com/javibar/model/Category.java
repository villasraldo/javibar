package com.javibar.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Category {
	
	@Id
	public ObjectId _id;
	public String codigo;
	public String nombre;
	
	public Category() {}
	public Category(String codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}
	public String get_id() {
		return _id.toHexString();
	}
	public void set_id(ObjectId _id) {
		this._id = _id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
