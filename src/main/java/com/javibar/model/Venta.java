package com.javibar.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Venta {
	
	@Id
	ObjectId _id;
	String codigo;
	Date fecha;
	Double monto;
	Integer nocierre;
	
	public Venta(String codigo, Date fecha, Double monto, Integer nocierre) {
		this.codigo = codigo;
		this.fecha = fecha;
		this.monto = monto;
		this.nocierre = nocierre;
	}
	public String get_id() {
		return _id.toHexString();
	}
	public void set_id(ObjectId _id) {
		this._id = _id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Integer getNocierre() {
		return nocierre;
	}
	public void setNocierre(Integer nocierre) {
		this.nocierre = nocierre;
	}
	
}
