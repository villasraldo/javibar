package com.javibar.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Cierres {
	
	@Id
	ObjectId _id;
	Date fecha;
	Integer nocierre;
	
	public Cierres(Date fecha, Integer nocierre) {
		this.fecha = fecha;
		this.nocierre = nocierre;
	}
	public String get_id() {
		return _id.toHexString();
	}
	public void set_id(ObjectId _id) {
		this._id = _id;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Integer getNocierre() {
		return nocierre;
	}
	public void setNocierre(Integer nocierre) {
		this.nocierre = nocierre;
	}
	
}
