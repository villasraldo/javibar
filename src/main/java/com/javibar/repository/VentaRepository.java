package com.javibar.repository;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.javibar.model.Venta;

public interface VentaRepository extends MongoRepository<Venta, String> {
	Venta findBy_id(ObjectId _id);
	List<Venta> findByCodigoAndFechaAndNocierre(String codigo, Date fecha, Integer nocierre);
	List<Venta> findAllByNocierreOrderByFechaDesc(Integer nocierre, Date fecha, Sort sort);
	List<Venta> findAllByNocierreOrderByFechaAsc(Integer nocierre, Date fecha, Sort sort);
	List<Venta> findByFecha(Date fecha);
	
}
