package com.javibar.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.javibar.model.Category;

public interface CategoryRepository extends MongoRepository<Category, String> {
	Category findBy_id(ObjectId _id);
	Category findByCodigo(String codigo);
}
