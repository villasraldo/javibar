package com.javibar.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.javibar.model.Cierres;

public interface CierresRepository extends MongoRepository<Cierres, String> {
	List<Cierres> findAllByOrderByFechaDesc(Date fecha, Sort sort);
}
