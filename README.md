<h1><center>Bienvenido al sistema de Gestión para JaviBar</center></h1>

<p>Para ejecutarlo descargue la aplicación e importela en un proyecto nuevo 
de Maven en Eclipse, limpie e instale la instancia con el comando: 
<b>mvn clean install</b></p>

<p>Una vez terminada la ejecución procesa a ejecutar el sistema con el comando:
<b>mvn spring-boot:run</b></p>

<p>O bien puede proceder a descargar el <b>.jar</b> ubicado en la carpeta 
<b>Target</b></p>